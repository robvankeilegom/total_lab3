﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1
{

    public class Functions
    {
        private static SqlConnection conn = new SqlConnection("Server= 127.0.0.1; Database= LAB3; Integrated Security=False; User Id=lab3; Password=Lab301;");

        public static SqlDataReader doSelectQuery(String queryString)
        {
            List<SelectListItem> listAllCodes = new List<SelectListItem>();

            try
            {
                checkConn();

                conn.Open();

                SqlCommand query = new SqlCommand(queryString, conn);
                SqlDataReader myReader = query.ExecuteReader();

                
                return myReader;
            }
            catch (Exception e)
            {

            }

            return null;
        }
        public static int doUpdateQuery(String queryString)
        {
            try
            {
                checkConn();

                conn.Open();

                SqlCommand query = new SqlCommand(queryString, conn);
                return query.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                return -1;
            }
        }
        public static void checkConn()
        {
            if (conn.State == System.Data.ConnectionState.Open)
                conn.Close();
        }


        public static string printUnixTimeStamp()
        {
           return ((DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }

        public static LayoutsModel GetLayouts(string SelectedID = null, bool list = false, bool useid = false)
        {
            SqlDataReader result;
            string querystring = "SELECT [LABEL_NAME], [LABEL_CODE], [LABEL_ID] FROM ";
            if (list && SelectedID == null)
            {
                querystring += SETTINGS.DB_LABELSLIST;
            } else
            {
                querystring += SETTINGS.DB_LABELS;
                querystring += " WHERE CODE_INT='" + SelectedID + "'";
            }

            result = Functions.doSelectQuery(querystring);
            List<SelectListItem> lstAllLayouts = new List<SelectListItem>();

            while (result != null && result.Read())
            {

                SelectListItem newLayout = new SelectListItem();
                newLayout.Text = result["LABEL_NAME"].ToString();
                if (useid)
                {
                    newLayout.Value = result["LABEL_ID"].ToString();
                }
                else
                {
                    newLayout.Value = result["LABEL_NAME"].ToString();
                }
                

                lstAllLayouts.Add(newLayout);
            }
            if (lstAllLayouts != null && lstAllLayouts.Count != 0)
                lstAllLayouts.First().Selected = true;

            LayoutsModel Layouts = new LayoutsModel()
            {
                Layouts = lstAllLayouts
            };

            return Layouts;
        }

        public static PrintersModel GetPrinters()
        {
            SqlDataReader result;
            result = Functions.doSelectQuery("SELECT * FROM " + SETTINGS.DB_PRINTERS);

            List<SelectListItem> lstAllPrinters = new List<SelectListItem>();

            int i = 0;
            while (result != null && result.Read())
            {

                SelectListItem newPrinter = new SelectListItem()
                {
                    Text = result["PRINTER_NAME"].ToString(),
                    Value = result["PRINTER_NAME"].ToString(),
                };

                lstAllPrinters.Add(newPrinter);
                i++;
            }
            if (lstAllPrinters != null && lstAllPrinters.Count != 0)
                lstAllPrinters.First().Selected = true;

            PrintersModel Printers = new PrintersModel()
            {
                Printers = lstAllPrinters
            };

            return Printers;
        }

        public static ArticlesModel GetArticles(string codeID = null, bool all = false)
        {
            SqlDataReader result;

            string query = "SELECT CODE_INT, COMPLETE_CODE FROM " + SETTINGS.DATABASE_USED;
            if (!all)
                query += " WHERE ARTECO = 1";

            result = Functions.doSelectQuery(query);

                
            List<SelectListItem> lstAllArticles = new List<SelectListItem>();
            List<String> lstSelectedArticles = new List<String>();

            while (result != null && result.Read() && result.HasRows == true)
            {
                SelectListItem newArticle = new SelectListItem()
                {
                    Text = result["COMPLETE_CODE"].ToString(),
                    Value = result["CODE_INT"].ToString()
                };

                if (codeID == null)
                {
                    codeID = newArticle.Value;

                }
                if (codeID == newArticle.Value)
                {
                    newArticle.Selected = true;
                    lstSelectedArticles.Add(newArticle.Value);
                    
                }

                lstAllArticles.Add(newArticle);
               
            }

            ArticlesModel Articles = new ArticlesModel()
            {
                Articles = lstAllArticles,
                SelectedArticles = lstSelectedArticles
            };

            return Articles;
        }
    }
}